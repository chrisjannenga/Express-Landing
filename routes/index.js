var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index',
    { title: 'Desert Dog Ventures',
      menuItems: ['Home', 'About', 'Services', 'Contact']});
});

router.get('/services', function(req, res, next) {
  res.render('services', {title: 'Desert Dog Ventures | Services'});
});

router.get('/mission', function(req, res, next) {
  res.render('mission', {title: 'Desert Dog Ventures | Mission'});
});

router.get('/our-team', function(req, res, next) {
  res.render('our-team', {title: 'Desert Dog Ventures | Our Team'});
});

router.get('/contact-us', function(req, res, next) {
  res.render('contact-us', {title: 'Desert Dog Ventures | Contact Us'});
});




module.exports = router;
